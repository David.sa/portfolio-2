# portfolio

## Pésentation du projet portfolio
Le but de ce projet est d'utilisé les competences aquise en **HTML, BOOTSTRAP** et **CSS** durant les cours, et d'en developper de nouvelles grace a l'auto-formation.

## Languages utilisées
* **HTML**
* **CSS**
* **JAVASCRIPT**
* **BOOTSTRAP**

## Interet
1. Mise en pratique des connaissances

2. utilisation et apprentissage de **BOOTSTRAP**

## Modifications
Je m'y suis repris a plusieurs fois pour créer un portfolio qui me plait,
toutefois voici quelques modifications :
* Ajout d'une musique de fond.
* Ajout d'un effet sur le nom et le prenom.
* Ajout d'icone pour les liens.
* Modification de la couleur des liens

## Exemple de code utilisé

voici un exemple de mon code **HTML** qui represente la modelisation de mon menu.

<div id="portfolio">
                <div id="port-wrapper">
                  <div class="text-2">
                    <h2> <a href="#text-changer-1">A mon propos</a></h2>
                    <h2> <a href="#text-changer-2">Portfolio</a></h2>
                    <h2> <a href="compétences.html">Compétences</a></h2>
                    <h2> <a href="https://www.linkedin.com/in/david-salvador-1a8044178/" target="_blank"><i id="link" class="fab fa-linkedin"></i></a></h2>
                    <h2> <a href="mailto:sd.salvador.david@gmail.fr"><i id="env" class="far fa-envelope"></i></a></h2>
                    <h2> <a href="https://gitlab.com/David.sa" target="_blank"><i id="gitlab" class="fab fa-gitlab"></i></a></h2>
                    <div id="text-changer-1" class="content">
                        <div>
                      <img src="img/photo.jpg" width="150px" align="left"/>
                    </div>
                    <div>


## Difficultés

Les difficultées sont survenue a l'utilisation de **BOOTSTRAP** et de **JAVASCRIPT**, que je ne maitrisais pas au moment de la conception.

## Copyright

©2018-2019 DavidSalvador All right reserved.

(pour voir mes autre création rendez-vous sur [Gitlab](https://gitlab.com/dashboard/projects).)

